import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Note } from '../note';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NoteService }  from '../note.service';
import { MessageService } from '../message.service';
import { faListUl } from '@fortawesome/free-solid-svg-icons';
import { faTable } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faClipboardList} from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import { faPlus} from '@fortawesome/free-solid-svg-icons';
import { faPencilAlt} from '@fortawesome/free-solid-svg-icons';
import { faSave} from '@fortawesome/free-solid-svg-icons';
import { faEdit} from '@fortawesome/free-solid-svg-icons';
import { faUndo} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-note-detail',
  templateUrl: './note-detail.component.html',
  styleUrls: ['./note-detail.component.css']
})
export class NoteDetailComponent implements OnInit {
  
  @Input() note: Note;

  @Input() isCurrentViewList: boolean;

  @Input() isAddNewNote: boolean;

  @Input() isChange: boolean;

  @Output() onBack = new EventEmitter<boolean>();

  @Output() onSave = new EventEmitter<boolean>();

  faList = faListUl;

  faTable = faTable;
  
  faBars = faBars;

  faClipboardList = faClipboardList;

  faTrashAlt = faTrashAlt;

  faPlus = faPlus;

  faPencilAlt = faPencilAlt;

  faSave = faSave;

  faEdit = faEdit; 
  
  faUndo = faUndo;

  constructor(
    private log: MessageService,
    private route: ActivatedRoute,
    private noteService: NoteService,
    private location: Location
  ) {}

  ngOnInit(): void {
    if(!this.isCurrentViewList && !this.isAddNewNote) {
      this.getNote();
    }
      this.note={id: null, name: '', content: '', category:''};
   
  }
  
  getNote(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.noteService.getNote(id)
      .subscribe(note => this.note = note);
  }

  setFlag() {
    this.isAddNewNote = true; 
  }

  back( ) {
    this.isChange = false;
      if(this.isCurrentViewList) {
       this.onBack.emit();
     } 
      else  this.location.back();
  }

  delete(note: Note): void {
    this.log.add("Заметка была успешно удалена");
    this.noteService.deleteNote(note).subscribe();
  }

  save(): void {
    this.isChange = false;
    this.isAddNewNote = false;
    
    if (this.note.id) {
    this.noteService.updateNote(this.note)
      .subscribe(() => this.log.add("Заметка была успешно отредактирована"));
       this.note={id: null, name: '', content: '', category:''};
       this.onSave.emit();
    } else {
      this.noteService.addNote(this.note)
      .subscribe(()=>this.log.add("Заметка была успешно добавлена"));
      this.note={id: null, name: '', content: '', category:''};
      this.onSave.emit();
    }
  }
}
