import { Injectable } from '@angular/core';
import { Note } from './note';
import { Category} from './category';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin':'*' })
};

@Injectable({
  providedIn: 'root'
})
export class NoteService {
  
  private notesUrl = 'http://notes/api/notes';  // URL to web api
  private notesUrlCategory = 'http://notes/api/category';
  constructor(private http: HttpClient,
    private messageService: MessageService) {
     }

    private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
        console.error(error); // log to console instead
        this.log(`${operation} failed: ${error.message}`);
        return of(result as T);
      };
    }


  getNotes(): Observable<Note[]> {

    return this.http.get<Note[]>(this.notesUrl)
    .pipe(
      catchError(this.handleError('getNotes',[]))
    )
  }
  
   getCategories(): Observable<Category[]> {

     return this.http.get<Category[]>(this.notesUrlCategory)
     .pipe(
       catchError(this.handleError('getCategories',[]))
     )
   }
  getNote(id: number): Observable<Note> {
    const url = `${this.notesUrl}/notes/${id}`;
    return this.http.get<Note>(url, httpOptions);

  }
  
  addNote (note: Note): Observable<Note> {
    const url = `${this.notesUrl}/notes`;
    
    return this.http.post<Note>(url, note, httpOptions, 
).pipe(
      tap((note: Note) => this.log(`Добавлена заметка № ${note.id}`)),
      catchError(this.handleError<Note>('addNote'))
    );
  }
  
  addCategory (category: Category): Observable<Category> {
     const url = `${this.notesUrlCategory}/category`;
     return this.http.post<Category>(url, category, httpOptions, 
 ).pipe(
       catchError(this.handleError<Category>('addCategory'))
     );
      }

  private log(message: string) {
  }

  updateNote (note: Note): Observable<any> {
    
    const url = `${this.notesUrl}/notes/${note.id}`;
    return this.http.put(url, note, httpOptions).pipe(
      catchError(this.handleError<any>('updateNote'))
    );
  }

  deleteNote (note: Note | number): Observable<Note> {
    const id = typeof note === 'number' ? note : note.id;
    const url = `${this.notesUrl}/notes/${id}`;
    return this.http.delete<Note>(url, httpOptions);
  }

  deleteCategory (category: Category | number): Observable<Category> {
    const id = typeof category === 'number' ? category : category.id;
    const url = `${this.notesUrlCategory}/category/${id}`;
    return this.http.delete<Category>(url, httpOptions);
  }
}
 