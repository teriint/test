import { Component, OnInit, Input } from '@angular/core';
import { Note } from '../note';
import { NoteService } from '../note.service';
import { MessageService } from '../message.service';
import { faListUl } from '@fortawesome/free-solid-svg-icons';
import { faTable } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faClipboardList} from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import { faPlus} from '@fortawesome/free-solid-svg-icons';
import { faPencilAlt} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {

  faList = faListUl;

  faTable = faTable;
  
  faBars = faBars;

  faClipboardList = faClipboardList;

  faTrashAlt = faTrashAlt;

  faPlus = faPlus;

  faPencilAlt = faPencilAlt;

  isCurrentViewList: boolean;

  isAddNewNote: boolean = false;
  
  isChange: boolean = false;

  isMenu: boolean;

  selectedNote: Note;
  
  notes: Note[];
  
  isDeleted: boolean = false;

  constructor(private noteService: NoteService,
    private log: MessageService,
    ) { }

  ngOnInit() {
    this.getNotes();

  }

  onLoad() {
    this.selectedNote = Note[0];
   
  }
	
  onSave() {
    if(this.isCurrentViewList) {
    this.getNotes();
    }
  }

  onBack() {
    if(this.isCurrentViewList) {
      this.selectedNote = null;
      this.getNotes();
     } 
  }

  changeNote () {
    this.isChange = true;

  }

  onSelect(note: Note): void {
    this.selectedNote = note;
    this.getNotes();
  }

  setFlag() {
    this.isAddNewNote = true;

  }
  
  setViewNotes() {
    if (this.isCurrentViewList)
    this.isCurrentViewList = false;
    else this.isCurrentViewList = true;
  }

  setMenu () {
    if (this.isMenu)
    this.isMenu = false;
    else this.isMenu = true;
  }
  setList() {
    this.isCurrentViewList = true;
    
  }

  setCard() {
    this.isCurrentViewList = false;
  }

  getNotes(): void {
    this.isChange = false;
    this.isAddNewNote= false;
    this.noteService.getNotes()
        .subscribe((notes) => {
          this.notes =  notes;
        }
        );
  }

  delete(note: Note): void {
    this.isDeleted = true;
  
    this.log.add("Заметка была успешно удалена");
    
    this.noteService.deleteNote(note).subscribe();
  
    
  }
 
  add(): void {
    if(this.selectedNote) {
      this.selectedNote={id: null, name: '', content: '', category:''};
      this.isChange = true;
    }
    this.isAddNewNote = true;
    
    }
}
