import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotesComponent }      from './notes/notes.component';
import { NoteDetailComponent }  from './note-detail/note-detail.component';
import { CategoryComponent } from './category/category.component';

const routes: Routes = [
  { path: '', redirectTo: '/notes', pathMatch: 'full' },
  { path: 'detail/:id', component: NoteDetailComponent },
  { path: 'notes', component: NotesComponent },
  { path: 'category', component: CategoryComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
