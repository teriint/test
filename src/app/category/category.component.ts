import { Component, OnInit } from '@angular/core';
import { Category} from '../category';
import { NoteService } from '../note.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {



  
  category: Category;

  categories: Category[];

  constructor(private noteService: NoteService,
    private log: MessageService,
  ) {

   }

  ngOnInit() {
    this.getCategories();
  }

  getCategories(): void {
    this.noteService.getCategories()
        .subscribe((categories) => {
          this.categories =  categories;
        }
        );
  }

  onSelect(category: Category): void {
   
    this.getCategories();
  }

  
	
  onSave() {

    this. getCategories();

  }


  

  delete(category: Category): void {
  
    this.log.add("Заметка была успешно удалена");
    this.noteService.deleteCategory(category).subscribe();
  
    
  }

  // add(): void {
  //   if(this.selectedCategory) {
  //     this.selectedCategory={id: null, note_id: null, name: ''};
  //   }

  //   }
   add(): void {
    this.noteService.addCategory(this.category)
       .subscribe(()=>this.log.add("Заметка была успешно добавлена"));
     }
}
