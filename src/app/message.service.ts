import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  message = new Subject();
 
  
  add(message: string) {
    this.message.next(message);
  }
 
  clear() {
    this.message.next();
  }
  
}
