import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  messages = [];

  constructor(public messageService: MessageService) { }
  
  ngOnInit() {
    this.messageService.message.subscribe((message)=> {
      this.messages.push(message);
      setTimeout(()=> {
        this.messages = [];
      } , 1000);
    })
  }

}
