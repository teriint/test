import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const notes = [
        { id: 1, name: 'Разработать веб-приложение', content: 'До 19.07.18 необходимо выполнить проект, закрепить навыки и знания', category: 'Работа' },
        { id: 2, name: 'Выполнить упражнение №234', content: '12.07.18 - выполнить упражнение по математике №234', category: 'Школа' },
        { id: 3, name: 'Убрать в квартире', content: 'Обязательно. До 19:00 выполнить уборку, мыть не надо', category: 'Домашние' },
        { id: 4, name: 'Встретить Лену',  content: 'Просто не забыть', category: 'Личные' },
        { id: 5, name: 'Выполнить отчет по бюджету за июль', content: 'Не забыть рассчитать все, что указано в списке. Распечатать в двух экземплярах', category: 'Работа' },
        { id: 6, name: 'Подсчет яиц', content: 'До субботы в обязательном порядке провести подсчет яиц в инкубаторе', category: 'Работа' },
        { id: 7, name: 'Добавить новую заметку', content: '', category: '' }
    ];
    return {notes};
  }
}
